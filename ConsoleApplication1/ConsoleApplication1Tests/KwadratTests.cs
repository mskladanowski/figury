﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1.Tests
{
    [TestClass()]
    public class KwadratTests
    {
        private Kwadrat kwadrat;

        [TestInitialize]
        public void TestInitialize()
        {
            kwadrat = new Kwadrat(2, new Point(1, 1));
        }

        [TestMethod()]
        public void ZwrocPunktyTest()
        {
            Assert.AreEqual(1, kwadrat.ZwrocPunkty()[1].X);
            Assert.AreEqual(3, kwadrat.ZwrocPunkty()[1].Y);
        }

        [TestMethod()]
        public void ZwrocPunktyTestFails()
        {
            Assert.AreNotEqual(2, kwadrat.ZwrocPunkty()[1].X);
            Assert.AreNotEqual(2, kwadrat.ZwrocPunkty()[1].Y);
        }

        [TestMethod()]
        public void ObrotTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ObrotTestFails()
        {
            Assert.Fail();
        }
    }
}