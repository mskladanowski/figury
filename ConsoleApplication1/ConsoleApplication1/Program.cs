﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var kwadrat = new Kwadrat(3, new Point(2, 2));
            var okrag = new Okrag(4, new Point(1, 1));

            Console.WriteLine("Punkty Kwadrat");
            foreach(var punkt in kwadrat.ZwrocPunkty())
            {
                Console.WriteLine(punkt.ToString());
            }

            Console.WriteLine("Punkty Okrag");
            foreach (var punkt in okrag.ZwrocPunkty())
            {
                Console.WriteLine(punkt.ToString());
            }

            Console.ReadKey();
        }
    }

    public abstract class Figura
    {
        public abstract List<Point>ZwrocPunkty();
        public abstract List<Point> Obrot(int kat);
        protected Point PunktZaczepienia { get; set; }
        protected int Dlugosc { get; set; }
        protected Point PoczatekUkladu { get; set; }

        public Figura(int dlugosc, Point punktZaczepienia)
        {
            Dlugosc = dlugosc;
            PunktZaczepienia = punktZaczepienia;
            PoczatekUkladu = new Point(0, 0);
        }
    }

    public class Kwadrat : Figura
    {
        public Kwadrat(int dlugosc, Point punktZaczepienia)
            :base (dlugosc, punktZaczepienia)
        { }

        public override List<Point> ZwrocPunkty()
        {
            var result = new List<Point>();
            result.Add(PunktZaczepienia);
            result.Add(new Point(PunktZaczepienia.X, PunktZaczepienia.Y + Dlugosc));
            result.Add(new Point(PunktZaczepienia.X + Dlugosc, PunktZaczepienia.Y + Dlugosc));
            result.Add(new Point(PunktZaczepienia.X + Dlugosc, PunktZaczepienia.Y));

            return result;
        }

        public override List<Point> Obrot(int kat)
        {
            throw new NotImplementedException();
        }
    }

    public class Okrag : Figura
    {
        public Okrag(int dlugosc, Point punktZaczepienia)
            :base (dlugosc, punktZaczepienia)
        { }

        public override List<Point> ZwrocPunkty()
        {
            var result = new List<Point>();
            result.Add(new Point(PunktZaczepienia.X, PunktZaczepienia.Y - Dlugosc/2));

            result.Add(new Point(PunktZaczepienia.X - Dlugosc/2, PunktZaczepienia.Y));
            result.Add(new Point(PunktZaczepienia.X, PunktZaczepienia.Y + Dlugosc/2));
            result.Add(new Point(PunktZaczepienia.X + Dlugosc/2, PunktZaczepienia.Y));

            return result;
        }

        public override List<Point> Obrot(int kat)
        {
            throw new NotImplementedException();
        }
    }

    public class Point
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Point(float x, float y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"Punkt: {X}, {Y}";
        }
    }
}
