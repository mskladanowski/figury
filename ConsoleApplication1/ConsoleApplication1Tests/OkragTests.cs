﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1.Tests
{
    [TestClass()]
    public class OkragTests
    {
        private Okrag okrag;
        [TestInitialize]
        public void TestInitialize()
        {
            okrag = new Okrag(2, new Point(1, 1));
        }

        [TestMethod()]
        public void ZwrocPunktyTest()
        {
            Assert.AreEqual(1, okrag.ZwrocPunkty()[0].X);
            Assert.AreEqual(0, okrag.ZwrocPunkty()[0].Y);
        }

        [TestMethod()]
        public void ZwrocPunktyTestFails()
        {
            Assert.AreNotEqual(2, okrag.ZwrocPunkty()[0].X);
            Assert.AreNotEqual(3, okrag.ZwrocPunkty()[0].Y);
        }

        [TestMethod()]
        public void ObrotTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ObrotTestFails()
        {
            Assert.Fail();
        }
    }
}